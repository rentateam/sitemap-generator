<?php
namespace Astartsky\SitemapGenerator\Parameters;

class LastModifiedParameter implements ParameterInterface
{
    protected $lastModified;

    /**
     * @param string $lastModified
     */
    public function __construct($lastModified)
    {
        $this->lastModified = $lastModified;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return (string) $this->lastModified;
    }

    /**
     * @return string
     */
    public function getProcessedValue()
    {
        return (string) $this->lastModified;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "lastmod";
    }
}