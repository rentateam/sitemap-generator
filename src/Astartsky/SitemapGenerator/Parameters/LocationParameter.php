<?php
namespace Astartsky\SitemapGenerator\Parameters;

class LocationParameter implements ParameterInterface
{
    protected $url;

    /**
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
 * @return string
 */
    public function getValue()
    {
        return (string) $this->url;
    }

    /**
     * @return string
     */
    public function getProcessedValue()
    {
        return htmlentities($this->url);
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "loc";
    }
}