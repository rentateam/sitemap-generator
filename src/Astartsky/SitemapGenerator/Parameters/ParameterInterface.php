<?php
namespace Astartsky\SitemapGenerator\Parameters;

interface ParameterInterface
{
    /**
     * @return string
     */
    public function getValue();

    /**
     * @return string
     */
    public function getProcessedValue();

    /**
     * @return string
     */
    public function getKey();
}