<?php
namespace Astartsky\SitemapGenerator\UrlEntryProcessor;

use Astartsky\SitemapGenerator\Parameters\LocationParameter;
use Astartsky\SitemapGenerator\UrlEntry;

class BaseUrlPreprocessor implements UrlEntryProcessorInterface
{
    protected $newBaseUrl;

    /**
     * @param string $newBaseUrl
     */
    public function __construct($newBaseUrl) 
    {
        $this->newBaseUrl = $newBaseUrl;
    }

    /**
     * @param UrlEntry $urlEntry
     * @return UrlEntry
     */
    public function process(UrlEntry $urlEntry)
    {
        $urlEntry->addParameter(new LocationParameter(trim($this->newBaseUrl, "/") . $urlEntry->getParameter("loc")->getValue()));

        return $urlEntry;
    }
}